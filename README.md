# Co-Occurrence Matrices as 3D Shapes Descriptor #

## About ##

This repository contains the CPP implementation of the 3D Spatial Dependence Matrices (or Co-occurrence matrices). This method was described in many papers, such as in Haralick, Robert M., and Karthikeyan Shanmugam (1973 - on 2D images), and in Kurani A., Xu D. Furst J. and Raicu D. (2004 - on MRI images).

## Dependencies ##

This project uses the Point Cloud Library (PCL), available at http://pointclouds.org/ .

## How to use ##

The "include" folder contains the implementations of the voxel grid methods, the implementation of the descriptor.

To compute the descriptor for the University of Washington RGB-D dataset, use the following command on the project folder `bash scripts/run.sh`. To change the parameters, edit `scripts/run.sh`.

The editable parameters are:

* __DS_PATH__ : The path of the dataset relative to the root folder of the project.
* __VOXELGRID_SIZE__ : The integer size (dimensions) of the cubic voxel grid.
* __MAX_NUM_JOBS__ : The number of parallel classes that are computed simultaneously. Larger values imply in a slower computer, thus being nearly impossible to use it while processing data.
* __WEKA_FILES_PATH__ : The path where the resulting arff files will be written.

## Known issues ##

An usual issue happens when executing the script on Ubuntu 14.04 LTS: the generated binary file always returns SIGSEGV (issue reported at  https://github.com/PointCloudLibrary/pcl/issues/619 , access 31/07/2017). However, no problem
should occur when executing it on other machines.
