#ifndef __GUARD_POINTCUBE_FEATURES__
#define __GUARD_POINTCUBE_FEATURES__

#include "operations.cpp"
#include <cmath>

namespace vox
{
  enum e_directions { DIRECTION_ROW, DIRECTION_COLUMN, DIRECTION_DEPTH };

  enum e_sdm_directions{
    DIRECTION_0_45,
    DIRECTION_0_90,
    DIRECTION_0_135,
    DIRECTION_45_45,
    DIRECTION_45_90,
    DIRECTION_45_135,
    DIRECTION_90_45,
    DIRECTION_90_90,
    DIRECTION_90_135,
    DIRECTION_135_45,
    DIRECTION_135_90,
    DIRECTION_135_135,
    DIRECTION_M_0
  };

  // # Spatial Dependence Matrix
  template<typename T>
  class DescriptorSDM
  {

  private:
    VoxelGrid<T> grid;
    std::vector< std::vector<double> >
      sdm_0_45,
      sdm_0_90,
      sdm_0_135,
      sdm_45_45,
      sdm_45_90,
      sdm_45_135,
      sdm_90_45,
      sdm_90_90,
      sdm_90_135,
      sdm_135_45,
      sdm_135_90,
      sdm_135_135,
      sdm_m_0;
    int stride;
    size_t sdm_size;

    // > Returns the value that x is closest to.
    // > n stands for the n double values those are going to be compared with x
    double closestTo(double x, int n, ...)
    {
      va_list args; // Contains the list of parameters
      double val;
      double last_val = 100000;
      va_start(args, n);   // Initialize the list after the given parameter
      for(int i = 0; i < n; i++)
      {
        val = va_arg(args, double);   // reads the next param
        if( std::abs(x-val) < std::abs(x - last_val) )
          last_val = val;
      }
      va_end(args); // Must be used to return the function when use va_list obj
      return last_val;
    }

    void normalizeSDM()
    {
      vox::e_sdm_directions direction;
      std::vector< std::vector< double > > sdm = getSDM(direction);
      u_int count = 0;

      for(std::size_t i = 0; i < 4; i++)
      {
        double theta = 45.0 * (double)(i);
        for(std::size_t j = 1; j < 4; j++)
        {
          double phi = 45.0 * (double)(j);
          direction = this->getDirections(theta, phi);
          std::vector< std::vector< double > > sdm = getSDM(direction);

          count = 0;
          for(u_int i = 0; i < sdm_size; i++)
            for(u_int j = 0; j < sdm_size; j++)
              count += sdm[i][j];
          for(u_int i = 0; i < sdm_size; i++)
            for(u_int j = 0; j < sdm_size; j++)
              sdm[i][j] /= (double)count;
        }
      }
      direction = this->getDirections(0.0, 0.0);
      sdm = getSDM(direction);
      count = 0;
      for(u_int i = 0; i < sdm_size; i++)
        for(u_int j = 0; j < sdm_size; j++)
          count += sdm[i][j];
      for(u_int i = 0; i < sdm_size; i++)
        for(u_int j = 0; j < sdm_size; j++)
          sdm[i][j] /= (double)count;

      /*
      u_int count = 0;
      for(u_int i = 0; i < sdm_size; i++)
        for(u_int j = 0; j < sdm_size; j++)
          count += sdm_0_45[i][j];
      for(u_int i = 0; i < sdm_size; i++)
        for(u_int j = 0; j < sdm_size; j++)
        {
          sdm_0_45[i][j] /= (double)count;
          sdm_0_90[i][j] /= (double)count;
          sdm_0_135[i][j] /= (double)count;
          sdm_45_45[i][j] /= (double)count;
          sdm_45_90[i][j] /= (double)count;
          sdm_45_135[i][j] /= (double)count;
          sdm_90_45[i][j] /= (double)count;
          sdm_90_90[i][j] /= (double)count;
          sdm_90_135[i][j] /= (double)count;
          sdm_135_45[i][j] /= (double)count;
          sdm_135_90[i][j] /= (double)count;
          sdm_135_135[i][j] /= (double)count;
          sdm_m_0[i][j] /= (double)count;
        }
      */
    }

  public:

    DescriptorSDM(VoxelGrid<T> &n_grid, size_t sdm_size, size_t stride=1)
    {
      this->grid = n_grid;
      this->sdm_size = sdm_size;
      this->stride = stride;

      sdm_0_45.resize(sdm_size);
      sdm_0_90.resize(sdm_size);
      sdm_0_135.resize(sdm_size);
      sdm_45_45.resize(sdm_size);
      sdm_45_90.resize(sdm_size);
      sdm_45_135.resize(sdm_size);
      sdm_90_45.resize(sdm_size);
      sdm_90_90.resize(sdm_size);
      sdm_90_135.resize(sdm_size);
      sdm_135_45.resize(sdm_size);
      sdm_135_90.resize(sdm_size);
      sdm_135_135.resize(sdm_size);
      sdm_m_0.resize(sdm_size);

      for(size_t i = 0; i < sdm_size; i++)
      {
        sdm_0_45[i].resize(sdm_size);
        sdm_0_90[i].resize(sdm_size);
        sdm_0_135[i].resize(sdm_size);
        sdm_45_45[i].resize(sdm_size);
        sdm_45_90[i].resize(sdm_size);
        sdm_45_135[i].resize(sdm_size);
        sdm_90_45[i].resize(sdm_size);
        sdm_90_90[i].resize(sdm_size);
        sdm_90_135[i].resize(sdm_size);
        sdm_135_45[i].resize(sdm_size);
        sdm_135_90[i].resize(sdm_size);
        sdm_135_135[i].resize(sdm_size);
        sdm_m_0[i].resize(sdm_size);
        for(size_t j = 0; j < sdm_size; j++)
        {
          sdm_0_45[i][j] = 0;
          sdm_0_90[i][j] = 0;
          sdm_0_135[i][j] = 0;
          sdm_45_45[i][j] = 0;
          sdm_45_90[i][j] = 0;
          sdm_45_135[i][j] = 0;
          sdm_90_45[i][j] = 0;
          sdm_90_90[i][j] = 0;
          sdm_90_135[i][j] = 0;
          sdm_135_45[i][j] = 0;
          sdm_135_90[i][j] = 0;
          sdm_135_135[i][j] = 0;
          sdm_m_0[i][j] = 0;
        }
      }
    }

    void computeSDM()
    {
      for(size_t x = 0; x < sdm_size; x++)
      {
        for(size_t y = 0; y < sdm_size; y++)
        {
          for(int i = 0; i < grid.height(); i++)
          {
            for(int j = 0; j < grid.width(); j++)
            {
              for(int k = 0; k < grid.depth(); k++)
              {
                if(i + stride < grid.height())
                {
                  if(k + stride < grid.depth())
                  {
                    // D, 0, D
                    if(grid.at(i, j, k) == x && grid.at(i+stride, j, k+stride) == y)
                      sdm_0_45[x][y] = sdm_0_45[x][y] + 1;
                  }
                  // D, 0, 0
                  if(grid.at(i, j, k) == x && grid.at(i+stride, j, k) == y)
                    sdm_0_90[x][y] = sdm_0_90[x][y] + 1;
                  if(k - stride >= 0)
                  {
                    // D, 0, -D
                    if(grid.at(i, j, k) == x && grid.at(i+stride, j, k-stride) == y)
                      sdm_0_135[x][y] = sdm_0_135[x][y] + 1;
                  }
                  if(j + stride < grid.width())
                  {
                    if(k + stride < grid.depth())
                    {
                      // D, D, D
                      if(grid.at(i, j, k) == x && grid.at(i+stride, j+stride, k+stride) == y)
                        sdm_45_45[x][y] = sdm_45_45[x][y] + 1;
                    }
                    // D, D, 0
                    if(grid.at(i, j, k) == x && grid.at(i+stride, j+stride, k) == y)
                      sdm_45_90[x][y] = sdm_45_90[x][y] + 1;
                    if(k - stride >= 0)
                    {
                      // D, D, -D
                      if(grid.at(i, j, k) == x && grid.at(i+stride, j+stride, k-stride) == y)
                        sdm_45_135[x][y] = sdm_45_135[x][y] + 1;
                    }
                  }
                }
                if(i - stride >= 0 && j + stride < grid.width())
                {
                  if(k + stride < grid.depth())
                  {
                    // -D, D,D
                    if(grid.at(i, j, k) == x && grid.at(i-stride, j+stride, k+stride) == y)
                      sdm_135_45[x][y] = sdm_135_45[x][y] + 1;
                  }
                  // -D, D, 0
                  if(grid.at(i, j, k) == x && grid.at(i-stride, j+stride, k) == y)
                    sdm_135_90[x][y] = sdm_135_90[x][y] + 1;
                  if(k - stride >= 0)
                  {
                    // -D, D,-D
                    if(grid.at(i, j, k) == x && grid.at(i-stride, j+stride, k-stride) == y)
                      sdm_135_135[x][y] = sdm_135_135[x][y] + 1;
                  }
                }
                if(j + stride < grid.width())
                {
                  if(k + stride < grid.depth())
                  {
                    // 0, D, D
                    if(grid.at(i, j, k) == x && grid.at(i, j+stride, k+stride) == y)
                      sdm_90_45[x][y] = sdm_90_45[x][y] + 1;
                  }
                  // 0, D, 0
                  if(grid.at(i, j, k) == x && grid.at(i, j+stride, k) == y)
                    sdm_90_90[x][y] = sdm_90_90[x][y] + 1;
                  if(k - stride >= 0)
                  {
                    // 0, D, -D
                    if(grid.at(i, j, k) == x && grid.at(i, j+stride, k-stride) == y)
                      sdm_90_135[x][y] = sdm_90_135[x][y] + 1;
                  }
                }
                if(k+stride < grid.depth())
                {
                  // 0, 0, D
                  if(grid.at(i, j, k) == x && grid.at(i, j, k+stride) == y)
                    sdm_m_0[x][y] = sdm_m_0[x][y] + 1;
                }
              }
            }
          }
        }
      }
      normalizeSDM();
      return ;
    }

    // > Theta is the angle between rows and columns axes;
    // > Phi is the angle between columns and depth.
    e_sdm_directions getDirections(double theta, double phi)
    {
      theta = (theta < 0) ? theta + 180: theta;
      phi = (phi < 0) ? phi + 180: phi;

      int nearest_theta = (int)closestTo(theta, 4, 0.0, 45.0, 90.0, 135.0);
      int nearest_phi = (int)closestTo(phi, 4, 0.0, 45.0, 90.0, 135.0);
      e_sdm_directions direction;
      switch (nearest_theta)
      {
        case 0:
          switch (nearest_phi)
          {
            case 45:
              direction = DIRECTION_0_45;
            break;
            case 90:
              direction = DIRECTION_0_90;
            break;
            case 135:
              direction = DIRECTION_0_135;
            break;
            default:
              //debug("Theta = 0 || Phi = 0");
              direction = DIRECTION_M_0;
            break;
          }

        case 45:
          switch (nearest_phi)
          {
            case 45:
              direction = DIRECTION_45_45;
            break;
            case 90:
              direction = DIRECTION_45_90;
            break;
            case 135:
              direction = DIRECTION_45_135;
            break;
            default:
              //debug("Theta = 45 || Phi = 0");
              direction = DIRECTION_M_0;
            break;
          }
        break;
        case 90:
          switch (nearest_phi)
          {
            case 45:
              direction = DIRECTION_90_45;
            break;
            case 90:
              direction = DIRECTION_90_90;
            break;
            case 135:
              direction = DIRECTION_90_135;
            break;
            default:
              //debug("Theta = 90 || Phi = 0");
              direction = DIRECTION_M_0;
            break;
          }
        break;
        case 135:
          switch (nearest_phi)
          {
            case 45:
              direction = DIRECTION_135_45;
            break;
            case 90:
              direction = DIRECTION_135_90;
            break;
            case 135:
              direction = DIRECTION_135_135;
            break;
            default:
              //debug("Theta = 135 || Phi = 0");
              direction = DIRECTION_M_0;
            break;
          }
        break;
        default:
          //debug("Unknown theta");
          direction = DIRECTION_M_0;
        break;

      }
      return direction;
    }

    std::vector< std::vector<double> > getSDM(e_sdm_directions direction)
    {
      switch (direction)
      {
        case DIRECTION_0_45:
          return sdm_0_45;
        break;
        case DIRECTION_0_90:
          return sdm_0_90;
        break;
        case DIRECTION_0_135:
          return sdm_0_135;
        break;
        case DIRECTION_45_45:
          return sdm_45_45;
        break;
        case DIRECTION_45_90:
          return sdm_45_90;
        break;
        case DIRECTION_45_135:
          return sdm_45_135;
        break;
        case DIRECTION_90_45:
          return sdm_90_45;
        break;
        case DIRECTION_90_90:
          return sdm_90_90;
        break;
        case DIRECTION_90_135:
          return sdm_90_135;
        break;
        case DIRECTION_135_45:
          return sdm_135_45;
        break;
        case DIRECTION_135_90:
          return sdm_135_90;
        break;
        case DIRECTION_135_135:
          return sdm_135_135;
        break;
        case DIRECTION_M_0:
          return sdm_m_0;
        break;
      }
    }

    float computeEntropy(e_sdm_directions direction)
    {
      std::vector< std::vector<double> > sdm = getSDM(direction);
      float sum = 0;

      for(int i = 0; i < sdm_size; i++)
      {
        for(int j = 0; j < sdm_size; j++)
        {
          sum = sum - (sdm[i][j] * log(sdm[i][j] + 0.000001f));
        }
      }
      return sum;
    }

    float computeEnergy(e_sdm_directions direction)
    {
      std::vector< std::vector<double> > sdm = getSDM(direction);
      float sum = 0;
      for(int i = 0; i < sdm_size; i++)
      {
        for(int j = 0; j < sdm_size; j++)
        {
          sum = sum + (sdm[i][j] * sdm[i][j]);
        }
      }
      return sum;
    }

    float computeContrast(e_sdm_directions direction)
    {
      std::vector< std::vector<double> > sdm = getSDM(direction);
      float sum = 0;
      for(int i = 0; i < sdm_size; i++)
      {
        for(int j = 0; j < sdm_size; j++)
        {
          sum = sum + (pow((i - j), 2) * sdm[i][j]);
        }
      }
      return sum;
    }

    float computeHomogeneity(e_sdm_directions direction)
    {
      std::vector< std::vector<double> > sdm = getSDM(direction);
      float sum = 0;
      for(int i = 0; i < sdm_size; i++)
      {
        for(int j = 0; j < sdm_size; j++)
        {
          sum = sum + (sdm[i][j]/(1 + abs(i - j)));
        }
      }
      return sum;
    }

    float computeSumMean(e_sdm_directions direction)
    {
      std::vector< std::vector<double> > sdm = getSDM(direction);
      float sum = 0;
      for(int i = 0; i < sdm_size; i++)
      {
        for(int j = 0; j < sdm_size; j++)
        {
          sum = sum + (i * sdm[i][j] + j * sdm[i][j]);
        }
      }
      return sum/2;
    }

    float computeVariance(e_sdm_directions direction, double avg)
    {
      std::vector< std::vector<double> > sdm = getSDM(direction);
      float var = 0;
      for(int i = 0; i < sdm_size; i++)
      {
        for(int j = 0; j < sdm_size; j++)
        {
          double p_ij = sdm[i][j];
          var += ( (pow(i - avg, 2) * p_ij) + ( pow( j - avg ,2) * p_ij ) );
        }
      }
      var = var / 2;
      return var;
    }

    // > TODO: Fix correlation (always = 1)
    float computeCorrelation(
      e_sdm_directions direction,
      double avg,
      double var
    )
    {
      std::vector< std::vector<double> > sdm = getSDM(direction);
      double corr = 0.0;
      for(int i = 0; i < sdm_size; i++)
      {
        for(int j = 0; j < sdm_size; j++)
        {
          corr += (i - avg) * (j - avg) * (sdm[i][j]);
        }
      }
      corr = corr / var;
      return 1.0;
    }

    float computeMaxOccurrence(e_sdm_directions direction)
    {
      std::vector< std::vector<double> > sdm = getSDM(direction);
      double max = -99.0;
      for(int i = 0; i < sdm_size; i++)
      {
        for(int j = 0; j < sdm_size; j++)
        {
          max = ( max < sdm[i][j]) ? sdm[i][j] : max;
        }
      }
      return max;
    }

    void computeAllFeatures(
      std::vector<double> &descriptor,
      e_sdm_directions direction
    )
    {
      double avg = 0.0;
      double var = 0.0;
      descriptor.push_back(computeEntropy(direction));
      descriptor.push_back(computeEnergy(direction));
      descriptor.push_back(computeContrast(direction));
      descriptor.push_back(computeHomogeneity(direction));
      avg = computeSumMean(direction);
      descriptor.push_back(avg);
      var = computeVariance(direction,avg);
      descriptor.push_back(var);
      //descriptor.push_back(computeCorrelation(direction,avg,var));
      descriptor.push_back(computeMaxOccurrence(direction));

      return ;
    }

  };

};

#endif
