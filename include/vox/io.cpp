#ifndef __GUARD_POINTCUBE_IO__
#define __GUARD_POINTCUBE_IO__

#include<iostream>
#include "core.cpp"

namespace vox
{
  template<typename T>
  void cube_debug(VoxelGrid<T> cube)
  {
    for(std::size_t k = 0; k < cube.depth(); k++)
    {
      std::cout << "Depth = " << k << std::endl;
      for(std::size_t i = 0; i < cube.height(); i++)
      {
        for(std::size_t j = 0; j < cube.width(); j++)
        {
          std::cout << static_cast<T>(cube.at(i,j,k)) << " ";
        }
        std::cout << std::endl;
      }
    }
  }

  void debug(std::string msg, bool show = true)
  {
    if(show)
      std::cout << msg << std::endl;
  }
};

#endif
