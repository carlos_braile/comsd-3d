#ifndef __GUARD_POINTCUBE_CORE__
#define __GUARD_POINTCUBE_CORE__

#include <vector>

namespace vox
{
  template<typename T>
  class VoxelGrid
  {
  private:
    std::size_t s_height, s_width, s_depth;
    std::vector<std::vector< std::vector< T > > >  grid;

  public:

    VoxelGrid(std::size_t s_height = 0 , std::size_t s_width = 0,
        std::size_t s_depth = 0, T value = 0)
    {
      resize(s_height, s_width, s_depth);
      fill(value);
    }

    void resize(std::size_t s_height, std::size_t s_width, std::size_t s_depth)
    {
      if( this->s_height != s_height || this->s_width != s_width ||
          this->s_depth != s_depth)
      {
        this->s_height = s_height;
        this->s_width = s_width;
        this->s_depth = s_depth;
        grid.resize(s_height);
        for(std::size_t i = 0; i < s_height; i++)
        {
          grid[i].resize(s_width);
          for(std::size_t j = 0; j < s_width; j++)
            grid[i][j].resize(s_depth);
        }
      }
      return ;
    }

    void fill(T value)
    {
      grid.resize(s_height);
      for(std::size_t i = 0; i < s_height; i++)
      {
        grid[i].resize(s_width);
        for(std::size_t j = 0; j < s_width; j++)
        {
          grid[i][j].resize(s_depth);
          for(std::size_t k = 0; k < s_depth; k++)
          {
            grid[i][j][k] = value;
          }
        }
      }
    }

    T& at(std::size_t i, std::size_t j, std::size_t k) { return grid[i][j][k]; }

    std::size_t height()  { return s_height; }

    std::size_t width()   { return s_width; }

    std::size_t depth()   { return s_depth; }

  };
};
#endif
