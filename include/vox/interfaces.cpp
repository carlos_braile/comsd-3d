#ifndef __GUARD_VOXELGRID_INTERFACES__
#define __GUARD_VOXELGRID_INTERFACES__

#include "core.cpp"

#include <limits>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>

namespace vox
{
  template<typename T>
  void cloud2voxels(
    VoxelGrid<T> &voxelgrid,
    pcl::PointCloud<pcl::PointNormal>::Ptr &cloud,
    int height,
    int width,
    int depth
  )
  {
    voxelgrid.resize(height, width, depth);
    std::size_t size = cloud->points.size();
    // 1. Get the minimum and maximum values from any point X, Y and Z of the cloud.
    double max[4] = {std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()};
    double min[4] = {std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()};
    for(size_t i = 0; i < size; ++i)
    {
      pcl::PointNormal p = cloud->points[i];
      if(p.x == p.x && p.y == p.y && p.z == p.z) // Verifies if values are NaN
      {
        if(p.x > max[0]) max[0] = p.x;
        if(p.y > max[1]) max[1] = p.y;
        if(p.z > max[2]) max[2] = p.z;

        if(p.x < min[0]) min[0] = p.x;
        if(p.y < min[1]) min[1] = p.y;
        if(p.z < min[2]) min[2] = p.z;

        double magnitude = sqrt( pow(p.normal_x, 2) + pow(p.normal_y, 2) + pow(p.normal_z, 2) );
        if(magnitude < min[3]) min[3] = magnitude;
        if(magnitude > max[3]) max[3] = magnitude;
      }
    }

    // 2. Apply max-min normalization and set the voxelgrid points
    // x,y,z axes at point clouds are col, row and depth on point voxelgrids
    for(size_t i = 0; i < size; ++i)
    {
      pcl::PointNormal p = cloud->points[i];
      if(p.x == p.x && p.y == p.y && p.z == p.z) // Verifies if values are NaN
      {
        std::size_t col = ((p.x - min[0])/(max[0] - min[0])) * (width - 1);
        std::size_t row = ((p.y - min[1])/(max[1] - min[1])) * (height - 1);
        std::size_t value = ((p.z - min[2])/(max[2] - min[2])) *  (depth - 1);
        double magnitude = sqrt( pow(p.normal_x, 2) + pow(p.normal_y, 2) + pow(p.normal_z, 2) );
        voxelgrid.at(row,col, value) = ((magnitude - min[3])/(max[3] - min[3])) * (256 - 1);
      }
    }

    return ;
  }

  template <typename T>
  void cloud2voxels(
    VoxelGrid<T> &voxelgrid,
    pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud,
    int height,
    int width,
    int depth
  )
  {
    voxelgrid.resize(height, width, depth);
    std::size_t size = cloud->points.size();
    // 1. Get the minimum and maximum values from any point X, Y and Z of the cloud.
    double max[3] = {std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()};
    double min[3] = {std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()};
    for(size_t i = 0; i < size; ++i)
    {
      pcl::PointXYZ p = cloud->points[i];
      if(p.x == p.x && p.y == p.y && p.z == p.z) // Verifies if values aren't NaN
      {
        if(p.x > max[0]) max[0] = p.x;
        if(p.y > max[1]) max[1] = p.y;
        if(p.z > max[2]) max[2] = p.z;

        if(p.x < min[0]) min[0] = p.x;
        if(p.y < min[1]) min[1] = p.y;
        if(p.z < min[2]) min[2] = p.z;
      }
    }

    // 2. Apply max-min normalization and set the voxelgrid points
    // x,y,z axes at point clouds are col, row and depth on point voxelgrids
    for(size_t i = 0; i < size; ++i)
    {
      pcl::PointXYZ p = cloud->points[i];
      if(p.x == p.x && p.y == p.y && p.z == p.z) // Verifies if values are NaN
      {
        std::size_t col = ((p.x - min[0])/(max[0] - min[0])) * (width - 1);
        std::size_t row = ((p.y - min[1])/(max[1] - min[1])) * (height - 1);
        std::size_t value = ((p.z - min[2])/(max[2] - min[2])) *  (depth - 1);
        // ! Binary voxelgrid (1 = point exists, 0 = point doesn't exist)
        voxelgrid.at(height - row - 1,width - col - 1, value) = 1;
      }
    }
    return ;
  }

  template <typename T>
  pcl::PointCloud<pcl::PointXYZ>::Ptr voxels2cloud(VoxelGrid<T> &voxelgrid)
  {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    std::size_t height, width, depth;

    height = voxelgrid.height();
    width = voxelgrid.width();
    depth = voxelgrid.depth();

    for(std::size_t i = 0; i < height; i++)
    {
      for(std::size_t j = 0; j < width; j++)
      {
        for(std::size_t k = 0; k < depth; k++)
        {
          T val = voxelgrid.at(i,j,k);
          if(val != 0)
          {
            pcl::PointXYZ p;
            p.x = j;
            p.y = i;
            p.z = k;
            cloud->points.push_back(p);
          }
        }
      }
    }
    return cloud;
  }

};
#endif
