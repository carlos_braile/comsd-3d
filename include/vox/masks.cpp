#ifndef __GUARD_POINTCUBE_MASKS__
#define __GUARD_POINTCUBE_MASKS__

#include"core.cpp"

namespace vox
{
  enum mask_type
  {
    TYPE_DIAGONAL_PLANE,
    TYPE_HORIZONTAL_PLANE,
    TYPE_VERTICAL_PLANE,
    TYPE_POINT,
    TYPE_LINE,
    TYPE_FULL
  };

  VoxelGrid<int> create_mask_dx()
  {
    VoxelGrid<int> mask(3,3,3);

    mask.at( 0 , 0 , 0 ) = 1;
    mask.at( 0 , 0 , 1 ) = 2;
    mask.at( 0 , 0 , 2 ) = 1;
    mask.at( 1 , 0 , 0 ) = 2;
    mask.at( 1 , 0 , 1 ) = 4;
    mask.at( 1 , 0 , 2 ) = 2;
    mask.at( 2 , 0 , 0 ) = 1;
    mask.at( 2 , 0 , 1 ) = 2;
    mask.at( 2 , 0 , 2 ) = 1;

    mask.at( 0 , 2 , 0 ) = -1;
    mask.at( 0 , 2 , 1 ) = -2;
    mask.at( 0 , 2 , 2 ) = -1;
    mask.at( 1 , 2 , 0 ) = -2;
    mask.at( 1 , 2 , 1 ) = -4;
    mask.at( 1 , 2 , 2 ) = -2;
    mask.at( 2 , 2 , 0 ) = -1;
    mask.at( 2 , 2 , 1 ) = -2;
    mask.at( 2 , 2 , 2 ) = -1;

    return mask;
  }

  VoxelGrid<int> create_mask_dy()
  {
    VoxelGrid<int> mask(3,3,3);

    mask.at( 0 , 0 , 0 ) = 1;
    mask.at( 0 , 0 , 1 ) = 2;
    mask.at( 0 , 0 , 2 ) = 1;
    mask.at( 0 , 1 , 0 ) = 2;
    mask.at( 0 , 1 , 1 ) = 4;
    mask.at( 0 , 1 , 2 ) = 2;
    mask.at( 0 , 2 , 0 ) = 1;
    mask.at( 0 , 2 , 1 ) = 2;
    mask.at( 0 , 2 , 2 ) = 1;

    mask.at( 2 , 0 , 0 ) = -1;
    mask.at( 2 , 0 , 1 ) = -2;
    mask.at( 2 , 0 , 2 ) = -1;
    mask.at( 2 , 1 , 0 ) = -2;
    mask.at( 2 , 1 , 1 ) = -4;
    mask.at( 2 , 1 , 2 ) = -2;
    mask.at( 2 , 2 , 0 ) = -1;
    mask.at( 2 , 2 , 1 ) = -2;
    mask.at( 2 , 2 , 2 ) = -1;

    return mask;
  }

  VoxelGrid<int> create_mask_dz()
  {
    VoxelGrid<int> mask(3,3,3);

    mask.at( 0 , 0 , 0 ) = 1;
    mask.at( 0 , 1 , 0 ) = 2;
    mask.at( 0 , 2 , 0 ) = 1;
    mask.at( 1 , 0 , 0 ) = 2;
    mask.at( 1 , 1 , 0 ) = 4;
    mask.at( 1 , 2 , 0 ) = 2;
    mask.at( 2 , 0 , 0 ) = 1;
    mask.at( 2 , 1 , 0 ) = 2;
    mask.at( 2 , 2 , 0 ) = 1;


    mask.at( 0 , 0 , 2 ) = -1;
    mask.at( 0 , 1 , 2 ) = -2;
    mask.at( 0 , 2 , 2 ) = -1;
    mask.at( 1 , 0 , 2 ) = -2;
    mask.at( 1 , 1 , 2 ) = -4;
    mask.at( 1 , 2 , 2 ) = -2;
    mask.at( 2 , 0 , 2 ) = -1;
    mask.at( 2 , 1 , 2 ) = -2;
    mask.at( 2 , 2 , 2 ) = -1;

    return mask;
  }

  VoxelGrid<unsigned char> create_mask_binary(std::size_t n, mask_type type)
  {
    VoxelGrid<unsigned char> mask(n, n, n);

    for(std::size_t alpha = 0; alpha < n; alpha++)
    {
      for(std::size_t beta = 0; beta < n; beta++)
      {
        for(std::size_t eta = 0; eta < n; eta++)
        {
          switch(type)
          {
            case TYPE_DIAGONAL_PLANE:
            {
              if( alpha == n - eta - 1)
                mask.at(alpha, beta, eta ) = 1;
              break;
            }
            case TYPE_FULL:
            {
              mask.at(alpha, beta, eta ) = 1;
              break;
            }
            case TYPE_HORIZONTAL_PLANE:
            {
              if(alpha == 1)
                mask.at(alpha, beta, eta ) = 1;
              break;
            }
            case TYPE_VERTICAL_PLANE:
            {
              if(eta == 1)
                mask.at(alpha, beta, eta ) = 1;
              break;
            }
            default:
              break;
          }
        }
      }
    }
    return mask;
  }
};
#endif
