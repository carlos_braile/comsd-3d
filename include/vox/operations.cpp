#ifndef __GUARD_POINTCUBE_OPERATIONS__
#define __GUARD_POINTCUBE_OPERATIONS__

#include "core.cpp"
#include <limits>
#include <cmath>

#include "io.cpp"

namespace vox
{
  enum padding_t
  {
    PADDING_FILL_ZEROS,
    PADDING_REPEAT_BORDERS
  };

  enum convolution_t
  {
    CONV_MIN_OPERATOR,
    CONV_MAX_OPERATOR,
    CONV_EQUALITY_OPERATOR,
    CONV_SUM
  };

  template <typename T>
  int getDifference(VoxelGrid<T> grid1, VoxelGrid<T> grid2)
  {
    int counter = 0;
    if(grid1.height() != grid2.height() || grid1.width() != grid2.width() || grid1.depth() != grid2.depth())
      return -1;
    for(size_t i = 0; i < grid1.height(); i++)
    {
      for(size_t j = 0; j < grid1.width(); j++)
      {
        for(size_t k = 0; k < grid1.depth(); k++)
        {
          if(grid1.at(i,j,k) != grid2.at(i,j,k))
            counter++;
        }
      }
    }
    return counter;
  }

/*
  template <typename TSource>
  void rotate(VoxelGrid<TSource> source, VoxelGrid<TSource> *dest,
      double angle_row, double angle_column, double angle_depth)
  {
    double C_gamma[3][3], C_beta[3][3], C_alpha[3][3], M[3][3], aux[3][3];

    // > Building the rotation matrix
    for(int i = 0; i < 3; i++)
      for(int j = 0; j < 3; j++)
      {
        C_gamma[i][j] = 0;
        C_beta[i][j] = 0;
        C_alpha[i][j] = 0;
        M[i][j] = 0;
        aux[i][j] = 0;
      }
    C_gamma[0][0] = cos(angle_row);
    C_gamma[0][1] = sin(angle_row);
    C_gamma[1][0] = -sin(angle_row);
    C_gamma[1][1] = cos(angle_row);
    C_gamma[2][2] = 1;

    C_beta[0][0] = 1;
    C_beta[1][1] = cos(angle_column);
    C_beta[1][2] = sin(angle_column);
    C_beta[2][1] = -sin(angle_column);
    C_beta[2][2] = cos(angle_column);

    C_alpha[0][0] = cos(angle_depth);
    C_alpha[0][1] = sin(angle_depth);
    C_alpha[1][0] = -sin(angle_depth);
    C_alpha[1][1] = cos(angle_depth);
    C_alpha[2][2] = 1;

    for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
      {
        for(int k = 0; k < 3; k++)
          aux[i][j] += C_gamma[i][k] * C_beta[k][j];
      }
    }

    for(int i = 0; i < 3; i++)
    {
      for(int j = 0; j < 3; j++)
      {
        for(int k = 0; k < 3; k++)
          M[i][j] += aux[i][k] * C_alpha[k][j];
      }
    }

    // > Finds the rotation of the current position
    for(int row = 0; row < 3; row++)
    {
      for(int col = 0; col < 3; col++)
      {
        for(int dep = 0; dep < 3; dep++)
        {
          int P[3];
          int P_line[3];
          P[0] = col;
          P[1] = row;
          P[2] = dep;
          for(int i = 0; i < 3; i++)
            P_line[i] = 0;

          for(int i = 0; i < 3; i++)
              for(int k = 0; k < 3; k++)
                P_line[i] += M[i][k] * P[k];
          std::string msg = "P = [" + std::to_string(P[1]) + "," +
              std::to_string(P[0]) + "," + std::to_string(P[2]) + "]";
          debug(msg);
          msg = "P' = [" + std::to_string(P_line[1]) + "," +
              std::to_string(P_line[0]) + "," + std::to_string(P_line[2]) + "]";
          debug(msg);

          //dest->at(P_line[1], P_line[0], P_line[2]) = source.at(row,col,dep);
        }
      }
    }
    return ;
  }
*/
  template <typename TSource>
  void quantize(VoxelGrid<TSource> source, VoxelGrid<unsigned int> *dest, unsigned int bin_size)
  {
    for(std::size_t k = 0; k < source.depth(); k++)
    {
      for(std::size_t i = 0; i < source.height(); i++)
      {
        for(std::size_t j = 0; j < source.width(); j++)
        {
          dest->at(i,j,k) = source.at(i,j,k) / bin_size;
        }
      }
    }
    return ;
  }

  template <typename TSource, typename TDest>
  void normalize(VoxelGrid<TSource> source, VoxelGrid<TDest> *dest, TDest max_value = 1)
  {
    std::size_t height = source.height();
    std::size_t width = source.width();
    std::size_t depth = source.depth();
    dest->resize(height, width, depth);

    // Finds max and min
    TSource max = std::numeric_limits<TSource>::min();
    TSource min = std::numeric_limits<TSource>::max();

    for(std::size_t k = 0; k < depth; k++)
    {
      for(std::size_t i = 0; i < height; i++)
      {
        for(std::size_t j = 0; j < width; j++)
        {
          TSource val = source.at(i,j,k);
          if(val > max)
            max = val;
          if(val < min)
            min = val;
        }
      }
    }

    // Max-min normalization
    for(std::size_t k = 0; k < depth; k++)
    {
      for(std::size_t i = 0; i < height; i++)
      {
        for(std::size_t j = 0; j < width; j++)
        {
          TDest val = source.at(i,j,k);
          val = ((val - min)* max_value)/(max - min) ;
          dest->at(i,j,k) = val;
        }
      }
    }
    return ;
  }

  template <typename TSource>
  void mirror(VoxelGrid<TSource> source, VoxelGrid<TSource> * dest)
  {
    std::size_t height = source.height();
    std::size_t width = source.width();
    std::size_t depth = source.depth();
    dest->resize(height, width, depth);

    for(std::size_t k = 0; k < depth; k++)
      for(std::size_t i = 0; i < height; i++)
        for(std::size_t j = 0; j < width; j++)
          dest->at(height - i - 1, width - j - 1, depth - k - 1) = source.at(i,j,k);

    return;
  }

  template <typename TSource, typename TDest>
  void threshold ( VoxelGrid<TSource> source, VoxelGrid<TDest> * dest ,TSource thresh_val )
  {
    std::size_t height = source.height();
    std::size_t width = source.width();
    std::size_t depth = source.depth();
    dest->resize(height, width, depth);

    for(std::size_t k = 0; k < depth; k++)
      for(std::size_t i = 0; i < height; i++)
        for(std::size_t j = 0; j < width; j++)
        {
          if(source.at(i,j,k) >= thresh_val)
            dest->at(i,j,k) = 1;
          else
            dest->at(i,j,k) = 0;
        }

    return ;
  }

  template<typename TSource, typename TDest>
  void getSubVoxelGrid(VoxelGrid<TSource> source, VoxelGrid<TDest> *dest , int s_row, int s_col, int s_depth,
                                std::size_t n_row, std::size_t n_col, std::size_t n_depth)
  {
    dest->resize(n_row, n_col, n_depth);

    for(int i = s_row; i - s_row < n_row; i++)
    {
      for(int j = s_col; j  - s_col < n_col; j++)
      {
        for(int k = s_depth; k - s_depth < n_depth; k++)
        {
          // > Out of bounds
          if( i < 0 || j < 0 || k < 0 ||
              i >= source.height() || j >= source.width() || k >= source.depth())
          {
            dest->at(i-s_row,j-s_col,k-s_depth) = 0;
          }
          // > Point of source
          else
          {
            dest->at(i-s_row,j-s_col,k-s_depth) = source.at(i,j,k);
          }
        }
      }
    }
    return ;
  }

  // > Given a voxelgrid of a voxel's region and a mask, compute only the value
  //    to that voxel
  template<typename T>
  int pointwiseConvolution( VoxelGrid<T> src,  VoxelGrid<T> mask,
      int i, int j, int k)
  {
    int value = 0;
    int radius = floor(mask.height()/2);
    for(int r = -radius; r <= radius; r++)
    {
      for(int c = -radius; c <= radius; c++)
      {
        for(int d = -radius; d <= radius; d++)
        {
          if( i + r >= 0 && i + r < src.height()  &&
              j + c >= 0 && j + c < src.width()   &&
              k + d >= 0 && k + d < src.depth()   )
            value += src.at(i+r, j+c, k+d) *
                mask.at(r + radius, c + radius, d + radius);
        }
      }
    }
    return value;
  }

  template<typename TSource, typename TMask , typename TDest>
  void convolution( VoxelGrid<TSource> source,
                    VoxelGrid<TDest> *dest,
                    VoxelGrid<TMask> mask,
                    convolution_t conv_type = CONV_SUM ,
                    padding_t padding_type = PADDING_FILL_ZEROS )
  {
    std::size_t height = source.height();
    std::size_t width  = source.width();
    std::size_t depth  = source.depth();
    std::size_t n      = mask.depth(); // It is expected that all mask's dimensions are equal
    std::size_t offset = static_cast<size_t>(n/2);
    dest->resize( height, width, depth );
    mirror(mask, &mask);
    for(size_t k = 0; k < depth; k++)
    {
      for(size_t i = 0; i < height; i++)
      {
        for(size_t j = 0; j < width; j++)
        {
          TDest sum = 0;
          for(size_t alpha = 0; alpha < n; alpha++)
          {
            for(size_t beta = 0; beta < n; beta++)
            {
              for(size_t eta = 0; eta < n; eta++)
              {
                TDest val;
                if(padding_type == PADDING_FILL_ZEROS)
                {
                  if( (i < (alpha + offset) ) || (j < (beta + offset) ) || (k < (eta + offset) ) ||
                      (i - alpha - offset >= height) || (j - beta - offset >= width) || (k - eta - offset >= depth) )
                    val = 0;
                  else
                    val = source.at(i - alpha - offset, j - beta - offset, k - eta - offset);
                }
                else if(padding_type == PADDING_REPEAT_BORDERS)
                {
                  std::size_t index_row = (i < (alpha + offset)) ? 0 : (i - alpha - offset);
                  std::size_t index_col = (j < beta + offset) ? 0 : (j - beta - offset) ;
                  std::size_t index_dep = (k < eta + offset) ? 0 : (k - eta - offset);

                  if(index_row >= height) index_row = height-1;
                  if(index_col >= width) index_col = width-1;
                  if(index_dep >= depth) index_dep = depth-1;

                  val = source.at(index_row, index_col, index_dep);
                }
                switch (conv_type)
                {
                  case CONV_SUM:
                    sum = sum + ( val * mask.at(alpha, beta, eta) );
                    break;
                  case CONV_EQUALITY_OPERATOR:
                    sum = sum && ( val == mask.at(alpha, beta, eta) );
                  break;
                  case CONV_MAX_OPERATOR:
                    sum = sum || ( val && mask.at(alpha, beta, eta) );
                  break;
                  case CONV_MIN_OPERATOR:
                    sum = sum && ( val && mask.at(alpha, beta, eta) );
                  break;
                }
              }
            }
          }
          dest->at(i,j,k) = sum;
        }
      }
    }
    return ;
  }
};

#endif
