#ifndef __GUARD_VOXELGRID_VISUALIZATION__
#define __GUARD_VOXELGRID_VISUALIZATION__

#include "core.cpp"

#include <limits>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/cloud_viewer.h>

namespace vox
{
  template<typename T>
  void showVoxelGrid(vox::VoxelGrid<T> grid , double dist, bool voxel_colors = false)
  {
    pcl::visualization::PCLVisualizer viewer("PCL Viewer");
    size_t counter = 0;
    for(size_t i = 0; i < grid.height(); i++)
    {
      for(size_t j = 0; j < grid.width(); j++)
      {
        for(size_t k = 0; k < grid.depth(); k++)
        {
          if(grid.at(i,j,k) > 0)
          {
            double _j = j;
            double _i = grid.height() - i - 1;
            double _k = grid.depth() - k - 1;
            double color = (voxel_colors) ? grid.at(i,j,k)/255.0 : 1.0 ;
            viewer.addCube(_j, (_j+dist), _i, _i+dist, _k, _k+dist, color, color, color, "cube" + std::to_string(counter++));
            counter++;
          }
        }
      }
    }
    while(!viewer.wasStopped ())
    {
      viewer.spinOnce ();
    }
  }

  template<typename T1, typename T2>
  void showTwoVoxelGrid(vox::VoxelGrid<T1> grid1,vox::VoxelGrid<T2> grid2, double dist, bool voxel_colors = false)
  {
    pcl::visualization::PCLVisualizer viewer("PCL Viewer");
    size_t counter = 0;
    for(size_t i = 0; i < grid1.height(); i++)
    {
      for(size_t j = 0; j < grid1.width(); j++)
      {
        for(size_t k = 0; k < grid1.depth(); k++)
        {
          if(grid1.at(i,j,k) > 0)
          {
            double _j = j;
            double _i = i;
            double _k = k;
            double color = (voxel_colors) ? grid1.at(i,j,k)/255.0 : 1.0 ;
            viewer.addCube(_j, (_j+dist), _i, _i+dist, _k, _k+dist, color, color, color, "cube" + std::to_string(counter++));
            counter++;
          }
        }
      }
    }
    for(size_t i = 0; i < grid2.height(); i++)
    {
      for(size_t j = 0; j < grid2.width(); j++)
      {
        for(size_t k = 0; k < grid2.depth(); k++)
        {
          if(grid2.at(i,j,k) > 0)
          {
            double _j = j;
            double _i = i;
            double _k = k;
            double color = (voxel_colors) ? grid2.at(i,j,k)/255.0 : 1.0 ;
            viewer.addCube(_j, (_j+dist), _i, _i+dist, _k, _k+dist, 0.1, 0.1, color, "cube" + std::to_string(counter++));
            counter++;
          }
        }
      }
    }
    while(!viewer.wasStopped ())
    {
      viewer.spinOnce ();
    }
  }

};
#endif
