#!/bin/bash

OUT_FILE_NAME=$1;
ARFF_PATH=$2

for ARFF_FILE in `ls ${ARFF_PATH}/*.arff`
do
	(cat $ARFF_FILE; echo '') >> $OUT_FILE_NAME;
done
