#!/bin/bash

# NUMERICAL VALUES DECLARATION
################################

declare -i MAX_NUM_JOBS;
declare -i TOTAL_ATTR_NUMBER;

################################

# PARAMETERS - CHANGEABLE
################################

# ! Algorithm parameters
VOXELGRID_SIZE=30;          # > Size of each voxelgrid dimension

# ! Script parameters
let "MAX_NUM_JOBS=2"        # > Number of simultaneous jobs running
DS_PATH="../rgbd-dataset/"; # > Change this for the
                            # location of your dataset
WEKA_FILES_PATH=".~arff/";  # > The location where the resulting
                            # arff files will be written

################################

# CONSTANTS
################################

DIRECTION_ATTR_NB=7;
NB_DIRECTIONS=13;

ARFF_CACHED_DATA=".~arff_temp_${VOXELGRID_SIZE}";
BUILD_PATH=".~build/";
RELATION_NAME="sdm_${VOXELGRID_SIZE}";
SCRIPTS_PATH="scripts/";

################################

# MAIN FLOW
################################

WEKA_FILE_NAME="${WEKA_FILES_PATH}${RELATION_NAME}.arff";

let "TOTAL_ATTR_NUMBER=DIRECTION_ATTR_NB * NB_DIRECTIONS";

bash ${SCRIPTS_PATH}run_dataset_UW.sh $DS_PATH $BUILD_PATH $VOXELGRID_SIZE $MAX_NUM_JOBS $ARFF_CACHED_DATA

if [ $? -eq 0 ]; then
  bash ${SCRIPTS_PATH}create_arff_header.sh $WEKA_FILE_NAME $TOTAL_ATTR_NUMBER $DS_PATH $RELATION_NAME;
  bash ${SCRIPTS_PATH}concatenate_arff_files.sh $WEKA_FILE_NAME $ARFF_CACHED_DATA;
fi

################################
