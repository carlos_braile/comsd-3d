#!/bin/bash
DS_PATH=$1
BUILD_PATH=$2
VGRID_SIZE=$3
MAX_NUM_JOBS=$4
ARFF_PATH=$5

# > Keep checking if the number of jobs is not higher than MAX_NUM_JOBS
# . Count the number of jobs
wait_jobs()
{
  declare -i JOBS_COUNT;
  let "JOBS_COUNT=0";
  JOBS=`jobs -p`
  for JOB in $JOBS
  do
    let "JOBS_COUNT+=1";
  done
  while [ "$JOBS_COUNT" -ge "$MAX_NUM_JOBS" ]
  do
    # . Count the number of jobs
    let "JOBS_COUNT=0";
    JOBS=`jobs -p`
    for JOB in $JOBS
    do
      let "JOBS_COUNT+=1";
    done
  done
}

# > Execute the program with the given parameters
class_iterator()
{
  local CLASS=$1;
  ${BUILD_PATH}main -p $DS_PATH -c $CLASS -v $VGRID_SIZE -a $ARFF_PATH;
}

# > Clear last run data
rm -rf ${ARFF_PATH} ;
mkdir ${ARFF_PATH} ;

# > Recompile, if necessary
if [ -d "$BUILD_PATH" ]; then
  cd ${BUILD_PATH} ;
  rm main ;
else
  mkdir ${BUILD_PATH} ;
  cd ${BUILD_PATH} ;
  cmake .. ;
fi

# > Recompile, if necessary
make ;

# > If 'make' failed: exit error
if [ $? -ne 0 ]; then
  exit -1 ;
fi

# > Return to project root dir
cd .. ;
for CLASS in `ls $DS_PATH`
do
  wait_jobs ;

  # > Execute
  class_iterator "$CLASS" &
done

# > Waits for all jobs to execute
wait ;

exit $? ;
