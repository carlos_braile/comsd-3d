// # INCLUDES
// #############################

// > STD
#include <iostream>
#include <fstream>
#include <ctime>

// > PCL
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/io.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/features/normal_3d.h>

// > VOX
#include "../include/vox/interfaces.cpp"
#include "../include/vox/operations.cpp"
#include "../include/vox/features.cpp"
#include "../include/vox/io.cpp"

#include "../include/dir.hpp"
#include "../include/weka.hpp"

// #############################

// # POINTCLOUD METHODS
// #############################

void preprocessPointCloud(
  pcl::PointCloud<pcl::PointXYZ>::Ptr &out_cloud,
  pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud
)
{
  // > Remove NaN data
  std::vector<int> nan_indices;
  pcl::removeNaNFromPointCloud(*cloud,*out_cloud, nan_indices);

  vox::debug("> Points: " + std::to_string(out_cloud->points.size()));
  return ;
}

// #############################

// # FEATURES METHODS
// #############################

template<typename T>
std::vector<double> computeDescriptor(
  std::vector<double> &descriptor,
  vox::VoxelGrid<T> vox_grid
)
{
  int count = 0;

  // > Co-Ocurrence Matrix descriptor
  vox::DescriptorSDM<int> c_sdm(vox_grid, 2);
  c_sdm.computeSDM();

  // > Return SDM features on each possible direction
  vox::e_sdm_directions direction;
  for(std::size_t i = 0; i < 4; i++)
  {
    double theta = 45.0 * (double)(i);
    for(std::size_t j = 1; j < 4; j++)
    {
      double phi = 45.0 * (double)(j);
      direction = c_sdm.getDirections(theta, phi);
      c_sdm.computeAllFeatures(descriptor, direction);
    }
  }
  direction = c_sdm.getDirections(0.0, 0.0);
  c_sdm.computeAllFeatures(descriptor, direction);

  return descriptor;
}


void getDescriptor(std::vector<double> &descriptor,
  std::string filepath,
  std::size_t grid_size
)
{
  // > Vars and stuff
  pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr proc_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  vox::VoxelGrid<int> vox_grid;

  // > Input
  vox::debug("=> 1. Reading" + filepath);

  if(pcl::io::loadPCDFile<pcl::PointXYZ> (filepath, *in_cloud) == -1)
  {
    PCL_ERROR ("Couldn't read file");
    exit(-1);
  }

  // > NaN removal
  vox::debug("=> 2. Data Preparation");
  vox::debug("-> 2.1 Preprocessing");
  preprocessPointCloud(proc_cloud, in_cloud);

  vox::debug("=> 3. Voxelization");
  vox::cloud2voxels<int>(vox_grid, proc_cloud, grid_size, grid_size, grid_size);

  vox::debug("=> 4. Descriptor");
  // > Descriptor computing
  computeDescriptor(descriptor, vox_grid);
  return ;
}

void getDatasetMNFiles(
  std::string ds_path,
  std::string selected_class,
  std::string instance_type,
  std::vector<std::string> &files,
  std::vector<std::string> &instances_classes
)
{
  std::vector<std::string> class_files;
  class_files = std::move(getFiles(ds_path + selected_class + "/" +
      instance_type + "/", DT_REG));
  for(std::string selected_file : class_files)
  {
    std::string complete_path = ds_path + selected_class + "/" +
        instance_type + "/" + selected_file;
    files.push_back(std::move(complete_path));
    instances_classes.push_back(selected_class);
  }
  return ;
}

void getDatasetUWFiles(
  std::string ds_path,
  std::string selected_class,
  std::vector<std::string> &files,
  std::vector<std::string> &instances_classes
)
{
  std::vector<std::string> poses;
  poses = std::move(getFiles(ds_path + selected_class + "/", DT_DIR));
  for(std::string pose : poses)
  {
    std::vector<std::string> class_files;
    class_files = std::move(getFiles(ds_path + selected_class + "/" +
        pose + "/", DT_REG));
    for(std::string selected_file : class_files)
    {
      std::string complete_path = ds_path + selected_class + "/" +
          pose + "/" + selected_file;
      files.push_back(std::move(complete_path));
      instances_classes.push_back(selected_class);
    }
  }
  return ;
}

// #############################
