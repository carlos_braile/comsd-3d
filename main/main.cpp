#include "header.cpp"

int main(int argc, char *argv[])
{
  std::cout << "0. Params" << std::endl;
  std::cout << std::endl;

  // > Default parameters
  std::string selected_class = "";
  std::string dataset_path = "";
  std::string arff_path = "";
  std::size_t voxelgrid_size = 0;

  // > Parameters reading
  int count = 1;

  while(count < argc)
  {
    // > Argument type
    std::string param = std::string(argv[count++]);
    // > Dataset default path
    if(!param.compare("-p"))
      dataset_path = std::string(argv[count++]);
    // > Select one class among all classes or all classes ("all")
    else if(!param.compare("-c"))
      selected_class = std::string(argv[count++]);
    // > Change the size of the voxelgrid
    else if(!param.compare("-v"))
      voxelgrid_size = atoi(argv[count++]);
    else if(!param.compare("-a"))
      arff_path = std::string(argv[count++]);
    else
      count++;
  }

  // > Parameters checking
  if( selected_class.compare("") || dataset_path.compare("") ||
      arff_path.compare("") || voxelgrid_size == 0)
  {
    std::cout  << "> Missing parameters. Finished." << std::endl;
    return -1;
  }

  // > Main flow
  std::vector< std::string > instances_classes;
  std::vector< std::string > dataset_classes;
  std::vector<std::string> files;
  std::ofstream w_file;
  std::string weka_file_name = "sdm-"+ selected_class+ "-" +
      std::to_string(voxelgrid_size);

  std::cout << "1. Entered main flow" << std::endl;

  // > Setup
  pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);

  // > Get all the dataset information (classes and instances)
  getDatasetUWFiles(dataset_path, selected_class, files, instances_classes);

  std::cout << "2. Got files" << std::endl;

  // > Start weka file
  w_file.open(arff_path + "/" + weka_file_name + ".arff");

  std::cout << "3. Opening weka file" << std::endl;
  // > Compute the descriptors from the objects in the dataset
  for(std::size_t i = 0; i < files.size(); i++)
  {
    std::string file = files[i];
    std::cout << "4. File: " << file << std::endl;
    std::vector<double> descriptor;

    // > Features computation
    getDescriptor(descriptor, file, voxelgrid_size);

    // > Put descriptor to ARFF
    appendDataToARFF(w_file, descriptor, instances_classes[i]);

    // > Progress output
    double progress = 100.0*((double)(i+1)) / ((double)files.size());
    std::cout << "> Progress: " << progress << "%" << std::endl;
    std::cout << "> Read " << (i+1) << " files from " << files.size() << std::endl;
  }
  w_file.close();
  return 0;
}
